var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');

router.get('/', userController.index)

router.get('/:id', userController.find)

router.post('/', userController.store)

router.put('/:id', userController.update)

router.delete('/:id', userController.destroy)

module.exports = router;