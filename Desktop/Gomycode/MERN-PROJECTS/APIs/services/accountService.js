const accountModel = require('../models/accountModel');

const index = async ()=>{

  try {
    const data = await accountModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id)=>{

  try {
    const data = await accountModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}

const store = async (body)=>{

  try {

    const accountSchema = new accountModel(body);
    await accountSchema.save();
    return accountSchema;
  } catch (error) {
return error
  }

}

const update = async (id, body)=>{

  try {
    await accountModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
return error
  }

}

const destroy = async (id, res)=>{

  try {
    await accountModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}