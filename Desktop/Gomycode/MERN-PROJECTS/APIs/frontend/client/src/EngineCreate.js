import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addEngine } from './reducers/engineReducer'
import { useNavigate } from 'react-router-dom';

const EngineCreate = () => {
  const [description, setDescription] = useState('')
  const [details, setDetails] = useState('')
  const [photo, setPhoto] = useState('')

const engines = useSelector((state) => state.engines.engines)
console.log(engines)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createEngine = (e) => {
e.preventDefault()
dispatch(addEngine({id: Date.now(), description, details, photo }))
Navigate('/engine')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Engine Information</h1>
    <form onSubmit={createEngine}>
      <div>
        <label htmlFor='description'>Description:</label>
        <input type='text' name='description' className='form-control' placeholder='Describe Your Engine' 
        onChange={e => setDescription(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='details'>Details:</label>
        <input type='text' name='details' className='form-control' placeholder='Enter Details' 
        onChange={e => setDetails(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
      </div><br/>
      <button className='btn btn-info'>Submit</button>
    </form>
    </div>
    </div>
  );
}

export default EngineCreate;
