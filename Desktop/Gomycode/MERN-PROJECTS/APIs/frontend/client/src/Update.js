import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { updateUser } from './reducers/userReducer';

const Update = () => {
  const {id} = useParams()
  const users = useSelector((state) => state.users.users)
  // const {name, email, photo} = existingUser[0]
const existingUser = users.filter(f => f.id == id)
const {name, email, photo} = existingUser[0]
const [uname, setName] = useState(name)
  const [uemail, setEmail] = useState(email)
  const [uphoto, setPhoto] = useState(photo)
  const dispatch = useDispatch()

  const handleUpdate = (e) => {
e.preventDefault()
dispatch(updateUser({
  id: id,
  name: uname,
  email: uemail,
  photo: uphoto
}))
  }

  return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <form onSubmit={handleUpdate}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' value={uname} 
          onChange={e => setName(e.target.value)} />
      </div>
      <div>
        <label htmlFor='email'>Email:</label>
        <input type='text' name='email' className='form-control' placeholder='Enter Email' value={uemail} 
          onChange={e => setEmail(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' value={uphoto} 
          onChange={e => setPhoto(e.target.value)}/>
        </div><br/>
      <button className='btn btn-info'>Update</button>
    </form>
    </div>
    </div>
  );
}

export default Update;
