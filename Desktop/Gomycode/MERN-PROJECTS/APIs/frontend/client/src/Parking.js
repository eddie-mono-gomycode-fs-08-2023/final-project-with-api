import React from 'react';
import './Home.css'
import Typewriter from './Typeriter';
import { Link } from 'react-router-dom';
import Footer from './Footer';
import ParkingDrop from './ParkingDrop';
import RequetCreate from './RequetCreate';

const Parking = () => {
  return (
    <div className='Home'>
    <div className='Home-nav'>
<Link to='/home'>Home</Link>
<Link to='/parking'>Parking Space</Link>
{/* <ParkingDrop/> */}
<Link to='/service'>Services</Link>
{/* <Link to='/users'>Users</Link> */}
<Link to='/'>Sign out</Link>

{/* <select>
  <option>Parking</option>
  <option>hello</option>
  <option>hello</option>
  <option>hello</option>
</select> */}

{/* <div className='Nav-right'>
        <div class="dropdowns">
  <span>Booking Records</span>
  <div class="dropdown-content">

  <a href='users'>Users</a>
  <br/>
  <Link to='users'>Use</Link>
  <br/>
  <a href='types'>Type</a>
  <br/>
  <br/>
  <a href='engine'>Engines</a>
  <br/>
  <br/>
  <a href='company'>Company</a>
  <br/>
  <br/>
<a href='space'>Parking Space</a>

  </div>
</div>       
</div>        */}


    </div>
      
    <section>
      <div className='ParkingTopImg'>
        <img src='https://content.jdmagicbox.com/comp/def_content/car-parking-management/cars-parked-in-parking-lot-car-parking-management-1-0stjw.jpg' alt='Car'/>
      </div>

      <div className='Parking'>
        {/* <input type='text' name='name' placeholder='Enter Your name'/>
        <input type='phone' name='telephone' placeholder='Enter Your Phone Number'/> */}
        {/* <button type='btn'>Request A Call</button> */}
        {/* <input type='submit' value='Request A Call'/> */}
        <RequetCreate/>
        {/* <Typewriter/> */}
        </div>
    </section>

    <div className='Home-gridContainer'>
    <div className='Home-flex'>
    <div className='Home-leftSide'>
    <img src='https://img.freepik.com/free-photo/parking_1127-2914.jpg' alt='parkTrack'/>
    </div>

    <div className='Home-rightSide'>
<h1>Our Methodology Is Exceptional. Try Us Out</h1> 

<hr className='hr'/>

<p>
Parking lots tend to be sources of water pollution because of their extensive impervious surfaces. 
Most existing lots have limited or no facilities to control runoff. Many areas today also require minimum 
landscaping in parking lots to provide.
</p>
<p>
Many municipalities require a minimum number of parking spaces, depending on the floor area in a store or 
the number of bedrooms in an apartment complex. In the US, each state's Department of Transportation sets 
the ratio.
</p>
    </div>

    </div>
    </div>
<Footer/>
    </div>
  );
}

export default Parking;
