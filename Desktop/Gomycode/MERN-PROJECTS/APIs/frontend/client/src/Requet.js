import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';
import { deleteRequet } from './reducers/RequetCallReducer';


const Requet = () => {
  const requets = useSelector((state) => state.requets.requets)
  console.log(requets);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deleteRequet({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    {/* <Link to='/engine'>Record Engine</Link> */}

<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h3>Call Requests</h3>
    <Link to='/requetCreate' className='btn btn-success my-3'>Record +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>TELEPHONE</th>
        </tr>
      </thead>
{requets.map((requet, index) => {
  return(
    <tr key={index}>
  <td>{requet.id}</td>
  <td>{requet.name}</td>
  <td>{requet.phone}</td>
  <td>

{/* <Link to={`/edit/${space.id}`} className='btn btn-sm btn-primary'>Edit</Link> */}
{/* <button onClick={() => handleDelete(space.id)} className='btn btn-sm btn-danger'>Delete</button> */}
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Requet;
