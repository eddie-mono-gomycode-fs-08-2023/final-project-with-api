import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteUser } from './reducers/userReducer';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';

const Engine = () => {
  const engines = useSelector((state) => state.engines.engines)
  console.log(engines);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deleteUser({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h3>Engine Record</h3>
    <Link to='/engineCreate' className='btn btn-success my-3'> Add Record +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPTION</th>
          <th>DETAILS</th>
          <th>PHOTO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{engines.map((engine, index) => {
  return(
    <tr key={index}>
  <td>{engine.id}</td>
  
  <td>{engine.description}</td>

  <td>{engine.details}</td>
  
  <td>{engine.photo}</td>
  <td>
<Link to={`/edit/${engine.id}`} className='btn btn-sm btn-primary'>Edit</Link>
<button onClick={() => handleDelete(engine.id)} className='btn btn-sm btn-danger'>Delete</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Engine;
