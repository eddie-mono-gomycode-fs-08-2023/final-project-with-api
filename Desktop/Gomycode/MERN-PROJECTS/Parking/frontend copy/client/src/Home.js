import React from 'react';
import './Home.css'
import Typewriter from './Typeriter';
import { Link } from 'react-router-dom';
import Footer from './Footer';

const Home = () => {
  return (
    <div className='Home'>
    <div className='Home-nav'>
<Link to='/home'>Home</Link>
<Link to='/parking'>Parking Space</Link>
<Link to='/service'>Services</Link>
<Link to='/users'>Users</Link>
<Link to='/'>Sign out</Link>
    </div>
      
    <section>
      <div className='HomeTopImg'>
        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1UQz5bJr8wmmX_a45fi2cwGBpqQDlDiX2BA&usqp=CAU' alt='Car'/>
      </div>

      <div className='Home-typeriter'>
        {/* <input type='text'></input> */}
        <Typewriter/>
        </div>
    </section>

    <div className='Home-gridContainer'>
    <div className='Home-flex'>
    <div className='Home-leftSide'>
    <img src='images/parkTrack.jpg' alt='parkTrack'/>
    </div>

    <div className='Home-rightSide'>
<h1>We Make Parking Easy And Affordable</h1> 

<hr className='hr'/>

<p>
Parking lots tend to be sources of water pollution because of their extensive impervious surfaces. 
Most existing lots have limited or no facilities to control runoff. Many areas today also require minimum 
landscaping in parking lots to provide.
</p>
<p>
Many municipalities require a minimum number of parking spaces, depending on the floor area in a store or 
the number of bedrooms in an apartment complex. In the US, each state's Department of Transportation sets 
the ratio.
</p>
    </div>

    </div>
    </div>
<Footer/>
    </div>
  );
}

export default Home;
