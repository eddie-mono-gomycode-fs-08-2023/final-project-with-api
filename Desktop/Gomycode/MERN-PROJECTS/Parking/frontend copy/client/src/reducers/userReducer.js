import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  users: [],
  user: {},
  
}

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    addUser: (state, action) => {
      
      state.users.push(action.payload)
    },
    updateUser: (state, action) => {
const {id, name, email, photo} = action.payload
const updateU = state.find(user => user.id == id)
if(updateU) {
  updateU.name = name
  updateU.email = email
  updateU.photo = photo

}
    },
    deleteUser: (state, action) => {
      const {id} = action.payload
const updateU = state.find(user => user.id === id)
if(updateU){
  return state.filter(f => f.id !== id)
}
    }
  }
})

export const {addUser, updateUser, deleteUser} = userSlice.actions
export default userSlice.reducer
