import './App.css';
import { Routes, Route } from 'react-router-dom';
import Navbar from './Navbar';
import Layout from './Layout';
import Home from './Home';
import Users from './Users';
import Create from './Create';
import Update from './Update';



function App() {
  return (

    <div className="App">
      <Navbar/>

<Routes>
<Route index element={<Layout/>} />
<Route path='/home' element={<Home/>}/>
<Route path='/users' element={<Users/>}/>
<Route path='/create' element={<Create/>}/>
<Route path='/edit/:id' element={<Update/>}/>

</Routes>


    </div>
  );
}

export default App;
