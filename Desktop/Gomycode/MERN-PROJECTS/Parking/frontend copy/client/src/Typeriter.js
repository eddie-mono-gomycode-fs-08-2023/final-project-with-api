import React from 'react';
import {useTypewriter, Cursor} from 'react-simple-typewriter'

const Typewriter = () => {
  const [TypeEffect] = useTypewriter({
    words: [' Welcome To Monopark', 'Our Clients Are Our Priority', 'We Are Trusted By Millions',
    'Choose Monopark Today', "We've The Best Deals For All Cars Parking Lot!"],
    loops: {},
    typeSpeed: 100,
    deleteSpeed: 50
  })
  return (
    <div>
      <h1>{TypeEffect}</h1>
    </div>
  );
}

export default Typewriter;
