import React, { useEffect, useState } from 'react';
import Data from './data.json'
import './Table.css'
import Navbar from '../Navbar';
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Provider } from "react-redux";


const Table = () => {
// I pass the Data I imported from data.json to the useState prop, so that I can have access to it
const [users, setUsers] = useState(Data);
const [name, setName] = useState('');
const [email, setEmail] = useState('');
const [phone, setPhone] = useState(''); 
const Navigate = useNavigate()
// useEffect(() => {
//   Axios.get('http://localhost:3300/spaces')
//   .then((respons) => {
//     setUsers(respons.data)
//   })
// },[])






const handleAdd = (e) => {

e.preventDefault()    
const newClient = {
id: Date.now(),
name,
email,
phone
}
setUsers(prevData => prevData.concat(newClient))
// setData([...Data, { name, email, phone }])    
setName('')
setEmail('')
setPhone('')
}

return (
<div>
<Navbar/>
<div className='AddFom'>
<br/>
<br/>
<br/>
<br/>
<div className='forms'>
<form>
{/* <input type='text' name='id' placeholder='Enter id'/> */}
<input type='text' name='name' value={name} placeholder='Enter Name' onChange={(e) => {setName(e.target.value)}}/>
<input type='text' name='email' value={email} placeholder='Enter Email' onChange={(e) => {setEmail(e.target.value)}}/>
<input type='text' name='phone' value={phone} placeholder='Enter Phone' onChange={(e) => {setPhone(e.target.value)}}/>
<button onClick={handleAdd}>Add</button>
</form>
</div>

<table>
<thead>
<th>id</th>
<th>Name</th>
<th>Surname</th>
<th>Telephone</th>
<th>Action</th>

</thead>


{users.map((user) => {
return(

<tr>
<td>{user.id}</td>
<td>{user.name}</td>
<td>{user.email}</td>
<td>{user.phone}</td>
<td>
<button className='Edit-btn' style={{backgroundColor: 'green', height: '48px', width: '50%', fontWeight: 'bold'}}>Edit</button>
<button className='Del-btn' style={{backgroundColor: 'red', height: '48px', width: '50%', fontWeight: 'bold'}}>Del</button>
<button className='Detail-btn' style={{width: '100%', backgroundColor: 'lightblue', color: 'black'}}
onClick={() => Navigate('/table/'+ user.id)}>details</button>
</td>
</tr>



)
} ) 
}

</table>
</div>


</div>
);
}



export default Table;
