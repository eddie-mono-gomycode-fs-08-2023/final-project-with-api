import React from 'react';

const Footer = () => {
  return (
    <div className='footer1'>
    <div className='Footer' style={{display: 'grid', backgroundColor: 'black', position: 'relative'}}>
      <div className='FooterItems' style={{display: 'flex', justifyContent: 'center', marginTop: '100px', gap: '20px'}}>
      <div style={{height: '400px', width: '30%', border: ''}}>
        <section style={{color: 'white', textAlign: 'center', fontWeight: 'bold'}}>
          <h2 style={{fontSize: '36px'}}>Monopark</h2>
          <p>We are the official providers of Airport parking. You can't park closer!</p>
        </section>
      </div>
      
      <div style={{height: '400px', width: '20%', border: ''}}>
      <section style={{color: 'white', textAlign: 'center',display: 'flex', flexDirection: 'column',}}>
      <h2 style={{fontSize: '36px', fontWeight: 'bold'}}>Navigation</h2>
        <ul>
          <li style={{display: 'flex', flexDirection: 'column', color: 'white'}}>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            </li>
        </ul>
      </section>
      </div>
      <div style={{height: '400px', width: '20%', border: ''}}>
      <section style={{color: 'white', textAlign: 'center',display: 'flex', flexDirection: 'column',}}>
      <h2 style={{fontSize: '36px', fontWeight: 'bold'}}>Contact Info</h2>
        <ul>
          <li style={{display: 'flex', flexDirection: 'column', color: 'white'}}>
            <a style={{color: 'white'}} href='home'>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            </li>
        </ul>
      </section>      
      </div>

      <div style={{height: '400px', width: '20%', border: ''}}>
      <section style={{color: 'white', textAlign: 'center',display: 'flex', flexDirection: 'column',}}>
      <h2 style={{fontSize: '36px', fontWeight: 'bold'}}>Contact Info</h2>
        <ul>
          <li style={{display: 'flex', flexDirection: 'column', color: 'white'}}>
            <a style={{color: 'white'}} href='home'>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            <a style={{color: 'white'}} href=''>Car Wash</a>
            </li>
        </ul>
      </section> 
      </div>
      </div>

    </div>
    </div>
  );
}

export default Footer;
