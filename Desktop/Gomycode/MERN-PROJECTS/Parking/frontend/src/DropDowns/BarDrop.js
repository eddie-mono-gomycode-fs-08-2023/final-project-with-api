import React, { useState } from 'react';

const BarDrop = () => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className='List-Drop1'>
        <button className='List-Drop' onClick={() => setIsOpen(!isOpen)}><h2>MENU</h2></button>
        {
    isOpen && (
      <div className='List-dropitems'>
      <div>
        <a href='space'>Regular</a> 
        </div>
        <div>
        <a href='account'>Premium</a> 
        </div>
        <div>
        <a href='moto'>Motorbike</a> 
        </div>

      </div>
    )
  }
    </div>
  );
}

export default BarDrop;
