var express = require('express')
var router = express.Router()
const loginController = require('../controllers/loginController');

// router.get('/', loginController.index);

router.get('/', loginController.find);

router.post('/', loginController.store);

// router.put('/:id', loginController.update);

// router.delete('/:id', loginController.destroy);

module.exports = router;