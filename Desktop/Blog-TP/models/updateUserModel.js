const mongoose = require('mongoose')

const updateUserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },

  email: {
    type: String,
    required: true
  },

  age: {
    type: Number,
    required: true
  },

  password: {
    type: String,
    minLength: 6,
    required: true
  },

  photo: {
    type: String,
    required: true
  }
})

const UserUpdate = mongoose.model('userUpdate', updateUserSchema);
module.exports = UserUpdate;