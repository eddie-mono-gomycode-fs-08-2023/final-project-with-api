var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')
var cors = require('cors');
var PORT = 3300;

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const loginRouter = require('./routes/loginRouter');
const signupRouter = require('./routes/signupRouter');
const blogPostRouter = require('./routes/blogPostRouter');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/signup', signupRouter);
app.use('/blogs', blogPostRouter)


// Sign up route
app.post('/register', (req, res) => {
  const {email, password} = req.body
bcrypt.hash(password, 10)
.then(hash => {
signupModel.create({email, password: hash})
.then(user => res.json('Success'))
.catch(err => res.json(err))
}).catch(err => res.json(err))
})




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// connect to server and database
  app.listen(PORT, (req, res) => {
    console.log(`App is running on port ${PORT}`)

    mongoose.connect('mongodb://127.0.0.1:27017/Blog-tp').
    catch(error => handleError(error));
    console.log('connected to db');

  });


module.exports = app;
