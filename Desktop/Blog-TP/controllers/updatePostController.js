const updatePostService = require('../models/updatePostService')

const updatePost = async (req, res) => {
  try {
    const data = await updatePostService.update(req.params.id, req.body);
    res.status(200).json(data)
  } catch (error) {
    res.status(error.staus).json({error: error.message})
}
};

module.exports = {updatePost};