const mongoose = require('mongoose');

const blogPostSchema = new mongoose.Schema({
  name:{
    type: String,
  },

  posts: {
    type: String
  }
},
{
  timestamps: true,
}
)

const BlogPost = mongoose.model('blogPost', blogPostSchema);
module.exports = BlogPost;