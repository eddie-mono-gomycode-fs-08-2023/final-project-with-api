var express = require('express')
var router = express.Router()
const signupController = require('../controllers/signupController');

router.get('/', signupController.index);

router.get('/:id', signupController.find);

router.get('/', signupController.find);

router.post('/', signupController.store);

router.put('/:id', signupController.update);

router.delete('/:id', signupController.destroy);

module.exports = router;