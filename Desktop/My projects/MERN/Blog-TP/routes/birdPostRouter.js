const express = require('express');
var router = express.Router();
const birdpostController = require('../controllers/birdPostController')


router.get('/', birdpostController.index);
router.post('/', birdpostController.store);
router.get('/:id', birdpostController.find)
router.post('/', birdpostController.store);
router.put('/:id', birdpostController.update);
router.delete('/:id', birdpostController.destroy);

module.exports = router;