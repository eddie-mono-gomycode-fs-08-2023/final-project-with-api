const birdPostModel = require('../models/birdPostModel');

const index = async ()=>{

  try {
    const data = await birdPostModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id)=>{

  try {
    const data = await birdPostModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}

const store = async (body)=>{

  try {

    const birdPostSchema = new birdPostModel(body);
    await birdPostSchema.save();
    return birdPostSchema;
  } catch (error) {
return error
  }

}

const update = async (id, body)=>{

  try {
    await birdPostModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
return error
  }

}

const destroy = async (id, res)=>{

  try {
    await birdPostModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}